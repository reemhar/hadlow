## Setup
pkg_list <- c("terra", "sf", "CDSE", "knitr", "ggplot2")
invisible(lapply(pkg_list, require, character.only = TRUE))
remotes::install_github("ropensci/rOPTRAM@dev_options")
library("rOPTRAM")
base_dir <- dirname(here::here())
GIS_dir <- file.path(base_dir, "GIS")
Data_dir <- file.path(base_dir, "Data")
Output_dir <- file.path(base_dir, "Output")
aoi <- sf::read_sf(file.path(GIS_dir, "ttayl_tarbush.gpkg"))

# OPTRAM preparation
optram_options("trapezoid_method", "polynomial", show_opts = FALSE)
optram_options("veg_index", "NDVI", show_opts = FALSE)
optram_options("plot_colors", "contours", show_opts = FALSE)
optram_options("rm.hi.str", TRUE, show_opts = FALSE)
optram_options("rm.low.vi", TRUE)
ttayl_VWC <- readRDS(
  file.path(Data_dir, "t-tayl", "VWC_2013-08-07_2023-10-01.rds"))
(min_date <- min(ttayl_VWC$Datetime))
(max_data <- max(ttayl_VWC$Datetime))
from_date <- "2020-10-01"
to_date <- "2023-10-01"
max_cloud = 8

## Run OPTRAM
# BOA_list <- optram_acquire_s2(aoi,
#                               from_date = from_date,
#                               to_date = to_date,
#                               max_cloud = max_cloud,
#                               output_dir = Output_dir)
# VI_dir <- file.path(Output_dir, getOption("optram.veg_index"))
# VI_list <- list.files(VI_dir, full.names = TRUE)
# STR_dir <- file.path(Output_dir, "STR")
# STR_list <- list.files(STR_dir, full.names = TRUE)
# VI_STR_df <- optram_ndvi_str(STR_list, VI_list,
#                              output_dir = Output_dir)

## Read prepared full_df
VI_STR_df <- readRDS(file.path(Output_dir, "VI_STR_data.rds"))
rmse <- optram_wetdry_coefficients(VI_STR_df, output_dir = Output_dir)
knitr::kable(rmse)

## Trapezoid plot
edges_df <- read.csv(file.path(Output_dir, "trapezoid_edges_poly.csv"))
pl <- plot_vi_str_cloud(VI_STR_df, edges_df, edge_points = TRUE)
pl <- pl + ggtitle("Trapezoid plot - Jornada", subtitle = "t-tayl plot")
pl

