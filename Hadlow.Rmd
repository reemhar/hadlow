---
title: "Hadlow_UK"
author: 'Reem Har Levi'
output: html_document
date: "2024-06-25"
--- 

<style type="text/css">
  body{
  font-size: 18pt;
}
</style>
 
## Setup 

```{r setup}
#library(usethis)
#library(sf)
#library(ggplot2)
pkg_list <- c("usethis", "sf", "terra", "ggplot2")
invisible(lapply(pkg_list, library, character.only = TRUE))
#install.packages("devtools")
devtools::install_github('ropensci/rOPTRAM')
library(rOPTRAM)
workdir <- dirname(here::here())
Data_dir <- file.path(workdir, "Data")
if (!dir.exists(Data_dir)) dir.create(Data_dir)
GIS_dir <- file.path(workdir, "GIS")

Output_dir <- file.path(workdir, "Output")
if (!dir.exists(Output_dir)) dir.create(Output_dir)
```

```{r}
#| eval: false
store_cdse_credentials(clientid="<...>", secret="<...>")
```

```{r}
optram_options("trapezoid_method", "exponential", show_opts = FALSE)
aoi <- sf::read_sf(file.path(GIS_dir, "hadlow.gpkg"))
from_date <- "2020-01-01"
to_date <- "2022-12-31"
```
```{r run-wrapper}
rmse <- optram(aoi, from_date = from_date, to_date = to_date,
              S2_output_dir = GIS_dir,
              data_output_dir = Output_dir)
```




